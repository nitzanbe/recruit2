@extends('layouts.app')
@section('content')
@section('title', 'Interview')

        <div><a href =  "{{url('/interviews/create')}}"> Add new nterview</a></div>
        <h1>Interviews</h1>
        <table class = "table table-dark">
        <tr> 
            <th>Id</th><th>Candidate</th><th>Interviewer</th><th>Date</th><th>Txt</th><th>Created</th><th>Updated</th>
        </tr>
        @foreach($interviews as $interview)
                <tr>
                    <td>{{$interview->id}}</td>

                    <td>
                        <div class="dropdown">
                            @if (isset($interview->candidate_id))    
                                {{$interview->candidate->name}}     
                            @else 
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if(isset($interview->candidate_id))
                                        {{$interview->candidate->name}}  
                                    @else
                                        Assign candidate
                                    @endif
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                @foreach($candidates as $candidate)
                                    <a class="dropdown-item" href="{{route('interview.changeCandidate',[$interview->id,$candidate->id])}}">{{$candidate->name}}</a>
                                @endforeach
                                </div> 
                            @endif
                        </div>                
                    </td>

                    <td>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if (isset($interview->user_id))
                                {{$interview->user->name}}
                                @else
                                    Define Interview
                                @endif
                            </button>                                                   
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                @foreach($users as $user)
                                <a class="dropdown-item" href="{{route('interview.changeUser',[$interview->id,$user->id])}}">{{$user->name}}</a>
                                @endforeach                               
                            </div>
                        </div>                            
                    </td> 

                    <td>{{$interview->date}}</td>
                    <td>{{$interview->txt}}</td>
                    <td>{{$interview->created_at}}</td>
                    <td>{{$interview->updated_at}}</td>
                </tr>
        @endforeach
        </table>
@endsection