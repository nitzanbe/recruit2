@extends('layouts.app')

@section('title', 'Create Candidate')

@section('content')
        <h1>Create Candidate</h1>
        <form method = "post" action = "{{action('CandidatesController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "name">Candiadte Name</label>
            <input type = "text" class="form-control" name = "name">
        </div>     
        <div class="form-group">
            <label for = "email">Candiadte Email</label>
            <input type = "text" class="form-control" name = "email">
        </div> 
        <div>
            <input class="btn btn-outline-secondary" type = "submit" name = "submit" value = "Create Candidate">
        </div>                       
        </form>    
@endsection
