@extends('layouts.app')

@section('title', 'Edit Candidate')

@section('content')       
       <h1>Edit Candidate</h1>
        <form method = "post" action = "{{action('CandidatesController@update',$candidate->id)}}">
        @csrf
        @METHOD('PATCH')
        <div class="form-group">
            <label for = "name">Candiadte Name</label>
            <input type = "text" class="form-control" name = "name" value = {{$candidate->name}}>
        </div>     
        <div class="form-group">
            <label for = "email">Candiadte Email</label>
            <input type = "text" class="form-control" name = "email" value = {{$candidate->email}}>
        </div> 
        <div>
            <input class="btn btn-outline-secondary" type = "submit" name = "submit" value = "Update Candidate">
        </div>                       
        </form>    
    </body>
</html>
@endsection
