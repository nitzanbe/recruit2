<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InterviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            [
                'date' => '2020-07-15',
                'txt' => 'The candidate was good',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'date' => '2020-07-14',
                'txt' => 'The candidate was good but not inaf',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],                   
        ]);   
    } 
}
