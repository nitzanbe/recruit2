<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interview;
use App\Candidate; 
use App\User;  
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class InterviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $interviews = Interview::all();
        $candidates = Candidate::all();
        $users = User::all();      
        return view('interviews.index', compact('interviews','candidates','users'));
    }

    public function changeCandidate($iid, $cid = null){
        $interview = Interview::findOrFail($iid);
        $interview->candidate_id = $cid;
        $interview->save(); 
        return redirect('interviews');
    }

    public function changeUser($iid, $uid = null)
    {
        $interview = Interview::findOrFail($iid);
        $interview->user_id = $uid;
        $interview->save();
        return redirect('interviews');
    } 
     

    public function myInterviews()
    {        
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $interviews = $user->interviews;
        $users = User::all();
        $candidates = Candidate::all();        
        return view('interviews.index', compact('interviews','users', 'candidates'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('new-initer');
        Gate::authorize('new-initer1');
        return view('interviews.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $interview = new Interview();
        $inter = $interview->create($request->all());
        $inter->user_id = Auth::id();
        $inter->save();
        return redirect('interviews');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
