<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    protected $fillable = ['date','txt'];

    public function candidate(){
        return $this->belongsTo('App\Candidate');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

}
